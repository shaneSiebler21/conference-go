from django.http import JsonResponse
from common.json import ModelEncoder
from events.models import Conference, Location
from .models import Attendee
from django.views.decorators.http import require_http_methods
import json


class LocationListEncoder(ModelEncoder):
    model = Location
    fields = ["name"]


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    fields = ["name"]


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    fields = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders = {
        "location": LocationListEncoder(),
    }


class AtendeeListEncoder(ModelEncoder):
    model = Attendee
    fields = ["name"]


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    fields = [
        "name",
        "email",
        "company_name",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceListEncoder(),
    }


def api_list_attendees(request, conference_id):
    attendees = Attendee.objects.filter(conference=conference_id)
    return JsonResponse(
        {"attendees": attendees},
        encoder=AtendeeListEncoder,
    )


def api_show_attendee(request, pk):
    """
    Returns the details for the Attendee model specified
    by the pk parameter.

    This should return a dictionary with email, name,
    company name, created, and conference properties for
    the specified Attendee instance.

    {
        "email": the attendee's email,
        "name": the attendee's name,
        "company_name": the attendee's company's name,
        "created": the date/time when the record was created,
        "conference": {
            "name": the name of the conference,
            "href": the URL to the conference,
        }
    }
    """
    attendee = Attendee.objects.get(id=pk)
    return JsonResponse(
        {
            "email": attendee.email,
            "name": attendee.name,
            "company_name": attendee.company_name,
            "created": attendee.created,
            "conference": {
                "name": attendee.name,
                "href": attendee.get_api_url(),
            },
        }
    )
